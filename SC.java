import java.util.Scanner;
class Student {     //定义学生类
     private String name;
     private String sex;  //私有成员变量
     private int age;
     public Student() {       //有参构造函数
         this.name = "tom";
         this.sex = "male";
         this.age = 15;
     } 
     public void toString(String n, int a, String s) {   //定义toString方法
         this.name = n;
         this.sex = s;
         this.age = a;
         System.out.println("Student [name='" + this.name + "', sex='" + this.sex + "', age=" + this.age + "]"); 
     } 
 } 
 public class SC {   //主函数
        public static void main(String[] args) {
         Scanner reader = new Scanner(System.in);
         String n = reader.next();
         int a = reader.nextInt();
         String s = reader.next();   
         Student smr = new Student();     //创建对象smr
         smr.toString(n, a, s);
         reader.close(); 
        }
 }