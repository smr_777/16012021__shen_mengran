import java.util.Scanner;
public class hw{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int height, width;
        char status;              //运用重载方法
        height = in.nextInt();
        width = in.nextInt();
        Board board = new Board(height, width);
        status = board.getStatus();
        System.out.print(status);
    }
}
class Board{
   int height, width;
   public Board(int height, int width){
       this.height = height;
       this.width = width;
   }
   public char getStatus(){
       int A=1,B=0;
       if(height<=width){
          return status(1);       
       }else{                    //定义不同类型的返回值
         return status(1.0);
       }
   }
   public char status(double rate){
     System.out.println("B");       //double为竖着放
     return 0;                       //返回主类输出B
  }
   public char status(int rate){
     System.out.println("A");      //int为平着放
     return 0;                     //返回主类输出A

   }
}