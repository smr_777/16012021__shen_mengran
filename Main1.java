public class Main1 {
	public static void main(String[] args) {
		Animal animal = new Dog();
		animal.shout();
		animal.run();
	}
}

class Animal {
	void shout() {
		System.out.println("animal shout！");
	}
	void run(){
	  System.out.println("animal run！");
	}
}

class Dog extends Animal {
	void shout() {
		super.shout();
		System.out.println("wangwang……");
	}

	void run() {
		System.out.println("Dog is running");
	}
}